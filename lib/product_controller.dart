import 'package:flutter/material.dart';

class ProductController extends StatelessWidget {
  final Function addProduct;
  final int productIndex;

  ProductController(this.addProduct, this.productIndex);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Theme.of(context).primaryColor,
      onPressed: () {
        addProduct(productIndex);
      },
      child: Text('Add Product'),
    );
  }
}
