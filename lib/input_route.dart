import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';

class InputRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InputRouteState();
  }
}

class InputRouteState extends State<InputRoute> {
  final TextEditingController keyEditController = TextEditingController();
  final TextEditingController valueEditController = TextEditingController();

  File jsonFile;
  Directory dir;
  Map<String, String> fileContent;
  bool fileExist = false;
  final String fileName = 'input_route.json';

  @override
  void initState() {
    super.initState();

    getApplicationDocumentsDirectory().then((Directory directory) {
      dir = directory;
      jsonFile = File(dir.path + '/' + fileName);

      jsonFile.exists().then((bool exist) {
        fileExist = exist;

        if (fileExist) {
          this.setState(() {
            fileContent = Map<String, String>.from(
                json.decode(jsonFile.readAsStringSync()));
          });
        }
      });
    });
  }

  @override
  void dispose() {
    keyEditController.dispose();
    valueEditController.dispose();
    super.dispose();
  }

  void createFile(
    Map<String, String> contents,
    Directory dir,
    String fileName,
  ) {
    File file = File(dir.path + '/' + fileName);

    file.createSync();
    fileExist = true;

    file.writeAsStringSync(json.encode(contents));
    setState(() {
      fileContent = contents;
    });
  }

  void writeToFile(String key, String value) {
    Map<String, String> contents = {key: value};

    if (fileExist) {
      Map<String, String> jsonFileContents =
          Map<String, String>.from(json.decode(jsonFile.readAsStringSync()));

      jsonFileContents.addAll(contents);
      jsonFile.writeAsStringSync(json.encode(jsonFileContents));

      setState(() {
        fileContent = jsonFileContents;
      });
    } else {
      createFile(contents, dir, fileName);
    }
  }

  void deleteFile(Directory dir, String fileName) {
    File file = File(dir.path + '/' + fileName);

    file.exists().then((bool exist) {
      fileExist = exist;
      if (fileExist) {
        fileExist = false;
        file.deleteSync();
      }
    });

    setState(() {
      fileContent = null;
    });
  }

  String getPrettyJSONString(jsonObject) {
    JsonEncoder encoder = JsonEncoder.withIndent("     ");
    return encoder.convert(jsonObject);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Input Route'),
        ),
        body: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 10.0),
            ),
            Text('JSON File content'),
            Padding(
              padding: EdgeInsets.only(top: 10.0),
            ),
            Text(getPrettyJSONString(fileContent)),
            Padding(
              padding: EdgeInsets.only(top: 10.0),
            ),
            TextField(
              decoration: InputDecoration(hintText: 'Enter Key'),
              controller: keyEditController,
            ),
            TextField(
              decoration: InputDecoration(hintText: 'Enter Value'),
              controller: valueEditController,
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.0),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                RaisedButton(
                  child: Text('Write To JSON File'),
                  onPressed: () => writeToFile(
                      keyEditController.text, valueEditController.text),
                ),
                RaisedButton(
                  child: Text('Delete JSON File'),
                  onPressed: () => deleteFile(dir, fileName),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
