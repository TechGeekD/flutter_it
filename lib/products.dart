import 'package:flutter/material.dart';

class Products extends StatelessWidget {
  final List<String> products;

  Products([this.products = const []]);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: products.length,
        itemBuilder: (BuildContext _context, int i) {
          return InkWell(
            onTap: () {
              Navigator.of(context).pushNamed('/newRoute');
            },
            child: Card(
              elevation: 8.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Column(
                children: <Widget>[
                  Image.asset('assets/flutter.png'),
                  Text(products[i]),
                ],
              )
            ),
          );
        },
      ),
    );
  }
}

