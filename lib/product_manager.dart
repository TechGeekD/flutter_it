import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

import 'package:flutter_it/products.dart';
import 'package:flutter_it/product_controller.dart';
import 'package:flutter_it/loader.dart';

class Post {
  final int userId;
  final int id;
  final String title;
  final String body;

  Post({this.userId, this.id, this.title, this.body});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
      body: json['body'],
    );
  }
}

class ProductManager extends StatefulWidget {
  final String initialProduct;

  ProductManager({this.initialProduct = 'Tester'});

  @override
  State<StatefulWidget> createState() {
    return _ProductManagerState();
  }
}

class _ProductManagerState extends State<ProductManager> {
  List<String> _products = [];
  bool isLoading = false;

  @override
  void initState() {
    _products.add(widget.initialProduct);
    super.initState();
  }

  @override
  void didUpdateWidget(ProductManager oldWidget) {
    print('[ProductManager State] didUpdateWidget()');
    super.didUpdateWidget(oldWidget);
  }

  _toggleLoader() {
    this.setState(() {
      this.isLoading = !this.isLoading;
    });
  }

  Future<Post> _getData(id) async {
    final http.Response response = await http.get(
      Uri.encodeFull('https://jsonplaceholder.typicode.com/posts/$id'),
      headers: {
        'Accept': 'application/json',
      },
    );
    return Post.fromJson(json.decode(response.body));
  }

  void _addProduct(int productId) {
    _toggleLoader();
    Timer(new Duration(seconds: 2), (){
      _getData(productId).then((response) {
        _toggleLoader();
        this.setState(() {
          this._products.add(response.title);
          this._products = this._products.reversed.toList();
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Loader(isLoading),
        Expanded(
          child: Products(_products),
        ),
        Container(
          margin: EdgeInsets.all(10.0),
          child: ProductController(_addProduct, _products.length),
        ),
      ],
    );
  }
}
