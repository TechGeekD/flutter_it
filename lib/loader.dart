import 'package:flutter/material.dart';

class Loader extends StatefulWidget {

  final bool isLoading;

  Loader(this.isLoading);

  @override
  State<StatefulWidget> createState() {
    return LoaderState();
  }
}

class LoaderState extends State<Loader> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1500));
    animation = CurvedAnimation(parent: controller, curve: Curves.elasticOut);
    animation.addListener(() {
      setState(() {});
    });
    animation.addStatusListener((AnimationStatus status) {
      print('**************** $status ****************');
    });
    controller.forward();
  }

  @override
  void didUpdateWidget(Loader oldWidget) {
    if(widget.isLoading) {
      controller.repeat();
    } else {
      controller.stop();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          color: Colors.white,
          height: 3.0,
          width: animation.value * 100.0,
        )
      ],
    );
  }
}
